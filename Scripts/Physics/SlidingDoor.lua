Script.openstate=false--bool "Start Open"
Script.distance=Vec3(0)--Vec3 "Distance"
Script.movespeed=1--float "Move speed" 0,100,3
Script.opensoundfile=""--path "Open Sound" "Wav File (*wav)|wav"
Script.loopsoundfile=""--path "Loop Sound" "Wav File (*wav)|wav"
Script.closesoundfile=""--path "Close Sound" "Wav File (*wav)|wav"
Script.closedelay=2000--int "Close delay"
Script.enabled=true--bool "Enabled"

function Script:Start()
	self.entity:SetGravityMode(false)
	if self.entity:GetMass()==0 then
		Debug:Error("Entity mass must be greater than 0.")
	end
	if self.opensoundfile~="" then self.opensound = Sound:Load(self.opensoundfile) end
	if self.loopsoundfile~="" then self.loopsound = Sound:Load(self.loopsoundfile) end
	if self.closesoundfile~="" then self.closesound = Sound:Load(self.closesoundfile) end
	
	self.opentime=0
	
	--Create a motorized slider joint
	local position=self.entity:GetPosition(true)
	local pin=self.distance:Normalize()
	self.joint=Joint:Slider(position.x,position.y,position.z,pin.x,pin.y,pin.z,self.entity,nil)
	if self.openstate then
		self.openangle=0
		self.closedangle=self.distance:Length()
	else
		self.openangle=self.distance:Length()
		self.closedangle=0
	end
	self.joint:EnableMotor()
	self.joint:SetMotorSpeed(self.movespeed)
end

function Script:Stop()
	if self.opensound then self.opensound:Release() end
	if self.loopsound then self.loopsound:Release() end
	if self.closesound then self.closesound:Release() end
	if self.loopsource then self.loopsource:Release() end
end

function Script:Open()--in
	if self.enabled then
		self.opentime = Time:GetCurrent()
		if self.openstate==false then
			if self.opensound then
				self.entity:EmitSound(self.opensound)
			end
			self.joint:SetAngle(self.openangle)
			self.openstate=true			
			self.component:CallOutputs("Open")
		end
	end
end

function Script:Close()--in
	if self.enabled then
		if self.openstate then
			if self.loopsource then
				self.loopsource:Release()
				self.loopsource=nil
			end
			if self.closesound then
				self.entity:EmitSound(self.closesound)
			end
			self.joint:SetAngle(self.closedangle)
			self.openstate=false
			self.component:CallOutputs("Close")
		end
	end
end

function Script:UpdatePhysics()
	if self.closedelay>0 then
		if self.openstate then
			local time = Time:GetCurrent()
			if time-self.opentime>self.closedelay then
				self:Close()
			end
		end
	else
		self.UpdatePhysics=nil--This will remove the function and it won't get called again
	end
end